/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bndtools.ace.framework;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.Manifest;

import org.amdatu.bndtools.ace.client.BndAceClient;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;
import org.osgi.framework.Constants;
import org.osgi.framework.FrameworkEvent;
import org.osgi.framework.launch.Framework;
import org.osgi.framework.launch.FrameworkFactory;
import org.osgi.util.tracker.ServiceTracker;

import aQute.launcher.constants.LauncherConstants;
import aQute.libg.classloaders.URLClassLoaderWrapper;

public class AceClientLauncher {

	private static final String FELIX_FRAMEWORK_JAR = "jar/org.apache.felix.framework-4.2.1.jar";
	private static final String FELIX_FRAMEWORK_FACRORY_CLASS = "org.apache.felix.framework.FrameworkFactory";

	private static final String FRAMEWORK_PROPERTIES_NAMESPACE = "org.amdatu.bndtools.ace.framework";

	private static final String FRAMEWORK_MODE_PROPERTY = FRAMEWORK_PROPERTIES_NAMESPACE + ".mode";

	private static final String ACE_LAUNCHER_PROPERTIES_CLIENT = "client.properties";
	private static final String ACE_LAUNCHER_PROPERTIES_SAIO = "server-allinone.properties";
	private static final String ACE_CONFIGURATION_RESOURCE_DIR = "conf/";
	
	private Framework m_framework;

	private BndAceClient m_client;

	public AceClientLauncher(Map<String, String> configuration) throws Exception {
		URL felixFrameworkJarURL = toFileURL(getClass().getClassLoader().getResource(FELIX_FRAMEWORK_JAR));
		URLClassLoaderWrapper urlClassLoaderWrapper = new URLClassLoaderWrapper(getClass().getClassLoader());
		urlClassLoaderWrapper.addURL(felixFrameworkJarURL);

		String mode = configuration.get(FRAMEWORK_MODE_PROPERTY);

		LauncherConstants launcherConstants;
		if ("client".equalsIgnoreCase(mode)) {
			launcherConstants = loadAceClientLauncherProperties(ACE_LAUNCHER_PROPERTIES_CLIENT);
		} else {
			launcherConstants = loadAceClientLauncherProperties(ACE_LAUNCHER_PROPERTIES_SAIO);
		}

		HashMap<String, String> aceConfiguration = prepareAceConfiguration(configuration, launcherConstants);

		Class<?> loadClass = getClass().getClassLoader().loadClass(FELIX_FRAMEWORK_FACRORY_CLASS);
		FrameworkFactory frameworkFactory = (FrameworkFactory) loadClass.newInstance();

		m_framework = frameworkFactory.newFramework(aceConfiguration);
		m_framework.start();

		List<Bundle> bundlesToStart = new ArrayList<>();
		BundleContext bundleContext = m_framework.getBundleContext();

		List<String> runBundles = launcherConstants.runbundles;
		for (String runBundle : runBundles) {
			URL resource = AceClientLauncher.class.getClassLoader().getResource(runBundle);
			if (resource == null) {
				System.out.println(runBundle + " seems to be missing");
				continue;
			}
			InputStream entry = resource.openStream();
			Bundle installBundle = bundleContext.installBundle(runBundle, entry);
			bundlesToStart.add(installBundle);
			entry.close();
		}

		URL test = getClass().getClassLoader().getResource("org.amdatu.bndtools.ace.client.impl.jar");
		Bundle installBundle = bundleContext
				.installBundle("org.amdatu.bndtools.ace.client.impl.jar", test.openStream());
		bundlesToStart.add(installBundle);

		try {
			for (Bundle bundle : bundlesToStart) {
				bundle.start();
			}
		} catch (Error | Exception e) {
			e.printStackTrace();
		}

		ServiceTracker<BndAceClient, BndAceClient> clientTracker = new ServiceTracker<>(bundleContext,
				BndAceClient.class, null);
		clientTracker.open();

		m_client = clientTracker.waitForService(15000l);
		clientTracker.close();
		if (m_client == null) {
			throw new RuntimeException("WorkspaceManager service not available");
		}
	}

	private LauncherConstants loadAceClientLauncherProperties(String name)
			throws IOException {
		URL resource = AceClientLauncher.class.getClassLoader().getResource(name);
		InputStream launcherProperties = resource.openStream();
		Properties properties = new Properties();
		properties.load(launcherProperties);
		LauncherConstants aceClientLauncherProperties = new LauncherConstants(properties);
		return aceClientLauncherProperties;
	}

	private HashMap<String, String> prepareAceConfiguration(Map<String, String> configuration,
			LauncherConstants launcherConstants) throws IOException {
		HashMap<String, String> config = new HashMap<>(launcherConstants.runProperties);
		config.put("org.osgi.framework.storage.clean", "onFirstInit");

		String httpPort = configuration.get("org.osgi.service.http.port");
		if (httpPort == null) {
			httpPort = "8080";
		}
		config.put("org.osgi.service.http.port", httpPort);

		String aceServer = configuration.get("org.apache.ace.server");
		if (aceServer == null) {
			aceServer = String.format("localhost:%s", httpPort);
		}
		config.put("org.apache.ace.server", aceServer);

		String aceObr = configuration.get("org.apache.ace.obr");
		if (aceObr == null) {
			aceObr = aceObr;
		}
		config.put("org.apache.ace.obr", aceObr);

		Enumeration<URL> manifestURLs = getClass().getClassLoader().getResources("META-INF/MANIFEST.MF");

		while (manifestURLs.hasMoreElements()) {
			URL manifestURL = (URL) manifestURLs.nextElement();
			if (!manifestURL.toString().contains("org.amdatu.bndtools.ace.framework")) {
				continue;
			}

			Manifest manifest = new Manifest(manifestURL.openStream());

			String exportPackageValue = manifest.getMainAttributes().getValue(Constants.EXPORT_PACKAGE);
			config.put("org.osgi.framework.system.packages.extra", exportPackageValue);
		}

		String storageBase;
		if (configuration.containsKey("launch.storage.dir")) {
			storageBase = configuration.get("launch.storage.dir").concat("/");
		} else {
			storageBase = "bnd-ace-framework-storage/";
		}

		config.put(org.osgi.framework.Constants.FRAMEWORK_STORAGE,
				storageBase.concat(launcherConstants.storageDir.getName()));

		if (configuration.containsKey("org.apache.ace.configurator.CONFIG_DIR")) {
			config.put("org.apache.ace.configurator.CONFIG_DIR",
					configuration.get("org.apache.ace.configurator.CONFIG_DIR"));
		} else {
			extractDefaultAceClientConfiguration(storageBase);

			String configDir = storageBase.concat("conf");
			config.put("org.apache.ace.configurator.CONFIG_DIR", configDir);
		}

		return config;
	}

	private void extractDefaultAceClientConfiguration(String configDir)
			throws IOException {
		File target = new File(configDir);
		target.mkdirs();

		URL resource = getClass().getClassLoader().getResource(ACE_CONFIGURATION_RESOURCE_DIR);
		String jarPath = resource.getPath().substring(ACE_CONFIGURATION_RESOURCE_DIR.length(), resource.getPath().indexOf("!"));

		try (JarFile jar = new JarFile(URLDecoder.decode(jarPath, "UTF-8"))) {
			Enumeration<JarEntry> entries = jar.entries();

			while (entries.hasMoreElements()) {
				JarEntry entry = entries.nextElement();
				String name = entry.getName();
				if (name.startsWith(ACE_CONFIGURATION_RESOURCE_DIR)) {
					File file = new File(target, name);
					if (entry.isDirectory()) {
						file.mkdirs();
					} else {
						Files.copy(jar.getInputStream(entry), file.toPath(), StandardCopyOption.REPLACE_EXISTING);
					}
				}
			}
		}
	}

	public FrameworkEvent waitForStop(long timeout) throws InterruptedException {
		return m_framework.waitForStop(timeout);
	}

	public void stop() throws BundleException {
		m_framework.stop();
	}

	private static URL toFileURL(URL resource) throws IOException {
		File f = File.createTempFile("resource", ".jar");
		f.getParentFile().mkdirs();
		InputStream in = resource.openStream();
		byte[] buffer = new byte[4096];
		try {
			OutputStream out = new FileOutputStream(f);
			try {
				int size = in.read(buffer);
				while (size > 0) {
					out.write(buffer, 0, size);
					size = in.read(buffer);
				}
			} finally {
				out.close();
			}
		} finally {
			in.close();
		}
		f.deleteOnExit();
		return f.toURI().toURL();
	}

	public BndAceClient getArtifactManager() {
		return m_client;
	}

}
