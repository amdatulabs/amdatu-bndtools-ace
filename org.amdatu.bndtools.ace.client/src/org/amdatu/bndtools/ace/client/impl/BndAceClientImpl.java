/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bndtools.ace.client.impl;

import static org.apache.ace.gogo.repo.RepositoryUtil.getIdentity;
import static org.apache.ace.gogo.repo.RepositoryUtil.getVersion;
import static org.apache.ace.gogo.repo.DeployerUtil.isSameBaseVersion;
import static org.apache.ace.gogo.repo.DeployerUtil.getNextSnapshotVersion;
import static org.apache.ace.gogo.repo.DeployerUtil.getBundleWithNewVersion;

import java.io.File;
import java.io.FileInputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import org.amdatu.bndtools.ace.client.BndAceBundle;
import org.amdatu.bndtools.ace.client.BndAceClient;
import org.apache.ace.bnd.repository.AceObrRepository;
import org.apache.ace.client.repository.object.Artifact2FeatureAssociation;
import org.apache.ace.client.repository.object.ArtifactObject;
import org.apache.ace.client.repository.object.Distribution2TargetAssociation;
import org.apache.ace.client.repository.object.DistributionObject;
import org.apache.ace.client.repository.object.Feature2DistributionAssociation;
import org.apache.ace.client.repository.object.FeatureObject;
import org.apache.ace.client.repository.stateful.StatefulTargetObject;
import org.apache.ace.client.workspace.Workspace;
import org.apache.ace.client.workspace.WorkspaceManager;
import org.apache.ace.gogo.repo.DeployerUtil;
import org.apache.ace.gogo.repo.RepositoryUtil;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Version;
import org.osgi.resource.Resource;

import aQute.bnd.osgi.Jar;
import aQute.bnd.service.RepositoryPlugin.PutResult;
import aQute.bnd.service.Strategy;

public class BndAceClientImpl implements BndAceClient {

	private volatile BundleContext m_bundleContext;
	private volatile WorkspaceManager m_workspaceManager;

	private ScheduledExecutorService m_executorService = Executors.newSingleThreadScheduledExecutor();

	private ScheduledFuture<?> m_scheduledFuture;

	private Object m_lock = new Object();

	private Set<BndAceBundle> m_bundlesToAdd = new HashSet<>();
	private Set<BndAceBundle> m_bundlesToRemove = new HashSet<>();

	private AceObrRepository m_aceObrRepository;
	private String m_featureName;

	@Override
	public void init(String featureName, String distributionName, String targetName) {
		m_featureName = featureName;
		try {
			initAceObrRepostiory();

			Workspace workspace = m_workspaceManager.cw();
			initFeature(workspace);
			initDistribution(workspace, distributionName, targetName);
			workspace.commit();
			m_workspaceManager.rw(workspace);
		} catch (Exception e) {
			throw new RuntimeException("Failed to initialize BndAceClient", e);
		}
	}

	private void initAceObrRepostiory() {
		m_aceObrRepository = new AceObrRepository();
		Map<String, String> props = new HashMap<String, String>();
		props.put(AceObrRepository.PROP_LOCATIONS,
				String.format("http://%s/obr/repository.xml", m_bundleContext.getProperty("org.apache.ace.obr")));
		m_aceObrRepository.setProperties(props);
	}

	private void initFeature(Workspace workspace) throws Exception {
		List<FeatureObject> lf = workspace.lf(String.format("(name=%s)", m_featureName));
		if (lf.isEmpty()) {
			workspace.cf(m_featureName);
		} else {
			List<Artifact2FeatureAssociation> la2f = workspace.la2f(String
					.format("(rightEndpoint=\\(name=%s\\))", m_featureName));
			for (Artifact2FeatureAssociation artifact2FeatureAssociation : la2f) {
				workspace.da2f(artifact2FeatureAssociation);
			}
		}
	}

	private void initDistribution(Workspace workspace, String distributionName, String targetName) throws Exception {
		List<DistributionObject> ld = workspace.ld(String.format("(name=%s)", distributionName));
		if (ld.isEmpty()) {
			workspace.cd(distributionName);
		}

		String f2dFilter = String.format("(&(leftEndpoint=\\(name=%s\\))(rightEndpoint=\\(name=%s\\)))", m_featureName,
				distributionName);
		List<Feature2DistributionAssociation> lf2d = workspace.lf2d(f2dFilter);
		if (lf2d.isEmpty()) {
			workspace.cf2d(String.format("(name=%s)", m_featureName), String.format("(name=%s)", distributionName));
		}

		if (workspace.lt(String.format("(id=%s)", targetName)).isEmpty()) {
			StatefulTargetObject t = (StatefulTargetObject) workspace.ct(targetName);
			t.setAutoApprove(true);
		}

		String d2tFilter = String.format("(&(leftEndpoint=\\(name=%s\\))(rightEndpoint=\\(id=%s\\)))",
				distributionName, targetName);
		List<Distribution2TargetAssociation> ld2t = workspace.ld2t(d2tFilter);

		if (ld2t.isEmpty()) {
			workspace.cd2t(String.format("(name=%s)", distributionName), String.format("(id=%s)", targetName));
		}
	}

	@Override
	public BndAceBundle add(File file) throws Exception {
		try (Jar jar = new Jar(file)) {
			String bsn = jar.getBsn();
			Version version = new Version(jar.getVersion());
			BndAceBundle bundle = new BndAceBundle(this, file, bsn, version);
			synchronized (m_lock) {
				m_bundlesToAdd.add(bundle);
				processChanges();
			}
			return bundle;
		}
	}

	@Override
	public void update(BndAceBundle bundle) {
		synchronized (m_lock) {
			m_bundlesToAdd.add(bundle);
			m_bundlesToRemove.remove(bundle);
			processChanges();
		}
	}

	@Override
	public void remove(BndAceBundle bundle) {
		synchronized (m_lock) {
			m_bundlesToAdd.remove(bundle);
			m_bundlesToRemove.add(bundle);
			processChanges();
		}

	}

	private void processChanges() {
		if (m_scheduledFuture != null && !m_scheduledFuture.isDone()) {
			m_scheduledFuture.cancel(false);
		}

		m_scheduledFuture = m_executorService.schedule(new WorkspaceUpdateProcessor(), 300, TimeUnit.MILLISECONDS);
	}

	private final class WorkspaceUpdateProcessor implements Runnable {
		@Override
		public void run() {
			synchronized (m_lock) {
				try {
					Workspace workspace = m_workspaceManager.cw();
					List<FeatureObject> lf = workspace.lf(String.format("(name=%s)", m_featureName));
					FeatureObject feature = lf.get(0);
					
					for (BndAceBundle bundle : m_bundlesToRemove) {
						System.out.println(String.format("Removing %s(%s)", bundle.getSymbolicName(), bundle.getVersion()));
						List<ArtifactObject> la = workspace.la(String.format("(Bundle-SymbolicName=%s)", bundle.getSymbolicName()));
						for (ArtifactObject a : la) {
							List<Artifact2FeatureAssociation> associations = a.getAssociationsWith(feature);
							for (Artifact2FeatureAssociation a2f : associations) {
								workspace.da2f(a2f);
							}
						}
					}
					m_bundlesToRemove.clear();
					
					for (BndAceBundle bundle : m_bundlesToAdd) {
						Version base = bundle.getVersion();

						List<org.osgi.resource.Resource> resources = RepositoryUtil.findResources(m_aceObrRepository,
								bundle.getSymbolicName());
						Resource matchedResource = null;
						for (Resource candidateResource : resources) {
							if (isSameBaseVersion(getVersion(candidateResource), base)
									&& (matchedResource == null || getVersion(matchedResource).compareTo(
											getVersion(candidateResource)) < 0)) {
								matchedResource = candidateResource;
							}
						}

						String location = null;
						String version = null;
						if (matchedResource == null) {
							System.out.println(String.format("Install %s(%s)", bundle.getSymbolicName(),
									bundle.getVersion()));

							PutResult put = m_aceObrRepository.put(
									new FileInputStream(bundle.getFile()),
									null);
							location = put.artifact.toString();
							version = bundle.getVersion().toString();
						} else {
							File repoFile = m_aceObrRepository.get(
									getIdentity(matchedResource),
									getVersion(matchedResource).toString(),
									Strategy.EXACT, null);

							Version nextVersion = getNextSnapshotVersion(getVersion(matchedResource));
							File bundleWithNewVersion = getBundleWithNewVersion(
									bundle.getFile(),
									nextVersion.toString());
							File repoFileWithNextVersion = getBundleWithNewVersion(
									repoFile, nextVersion.toString());

							if (DeployerUtil.jarsDiffer(bundleWithNewVersion, repoFileWithNextVersion)) {
								System.out.println(String.format("Update %s(%s -> %s)", bundle.getSymbolicName(),
										bundle.getVersion(), nextVersion));
								PutResult put = m_aceObrRepository
										.put(new FileInputStream(bundleWithNewVersion), null);
								location = put.artifact.toString();
								version = nextVersion.toString();
							} else {
								System.out.println(String.format("Install %s(%s)", bundle.getSymbolicName(),
										bundle.getVersion()));
							}
							bundleWithNewVersion.delete();
							repoFileWithNextVersion.delete();
						}
						
						String artifactName;
						if (version != null) {
							artifactName = String.format("%s-%s", bundle.getSymbolicName(), version);
							workspace.ca(artifactName.toString(), location, bundle.getSymbolicName(), version);
						} else {
							artifactName = String.format("%s-%s", bundle.getSymbolicName(), base);
							List<ArtifactObject> la = workspace.la(String.format("(artifactName=%s)", artifactName));
							if (la.isEmpty()) {
								workspace.ca(artifactName.toString(), location, bundle.getSymbolicName(),
										base.toString());
							}
						}
						
						List<ArtifactObject> la = workspace.la(String.format("(Bundle-SymbolicName=%s)", bundle.getSymbolicName()));
						for (ArtifactObject a : la) {
							List<Artifact2FeatureAssociation> associations = a.getAssociationsWith(feature);
							for (Artifact2FeatureAssociation a2f : associations) {
								workspace.da2f(a2f);
							}
						}
						
						workspace.ca2f(String.format("(artifactName=%s)", artifactName),
								String.format("(name=%s)", feature.getName()));
											
					}
					m_bundlesToAdd.clear();

					

					workspace.commit();
					m_workspaceManager.rw(workspace);
				} catch (Error | Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

}
